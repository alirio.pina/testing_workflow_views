package com.cat2.sd;

import com.cat2.questions.TheScreenShouldHas;
import com.cat2.setup.SetUp;
import com.cat2.tasks.login.SuccessfullyLogIn;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

public class StartUpViewSD extends SetUp {

    @Before
    public void before(){
        OnStage.setTheStage(new OnlineCast());
        actor.can(BrowseTheWeb.with(hisMobileDevice));
    }

    @Given("the user has access in the Start Up screen")
    public void theUserHasAccessInTheStartUpScreen() {

        actor.attemptsTo(
                SuccessfullyLogIn.successfullyLogIn()
        );

    }
    @Then("he can see all the options")
    public void heCanSeeAllTheOptions() {

        actor.should(
                seeThat(TheScreenShouldHas.startDayOption(), containsString("Start Day")),
                seeThat(TheScreenShouldHas.resumeDayOption(), containsString("Resume Day")),
                seeThat(TheScreenShouldHas.deleteDayOption(), containsString("Delete Day")),
                seeThat(TheScreenShouldHas.reportsOption(), containsString("Reports")),
                seeThat(TheScreenShouldHas.closeDayOption(), containsString("Close Day")),
                seeThat(TheScreenShouldHas.uploadDayOption(), containsString("Upload Day")),
                seeThat(TheScreenShouldHas.printerOption(), containsString("Printer")),
                seeThat(TheScreenShouldHas.maintenanceOption(), containsString("Maintenance"))
        );

    }


}
