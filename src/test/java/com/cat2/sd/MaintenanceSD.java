package com.cat2.sd;

import com.cat2.questions.TheScreenShouldHas;
import com.cat2.setup.SetUp;
import com.cat2.tasks.login.SuccessfullyLogIn;
import com.cat2.tasks.maintenance.MaintenanceAllOptions;
import com.cat2.tasks.maintenance.MaintenanceSomeOptions;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

public class MaintenanceSD extends SetUp {

    @Before
    public void before(){
        OnStage.setTheStage(new OnlineCast());
        actor.can(BrowseTheWeb.with(hisMobileDevice));
    }

    @Given("the user is in the maintenance screen.")
    public void theUserIsInTheMaintenanceScreen() {

        actor.attemptsTo(
                SuccessfullyLogIn.successfullyLogIn()
        );

    }

    @When("the user selects all options.")
    public void theUserSelectsAllOptions() {

        actor.attemptsTo(
                MaintenanceAllOptions.maintenanceSelection()
        );

    }

    @Then("he can sees all the results.")
    public void heCanSeesAllTheResults() {

        actor.should(
                seeThat(TheScreenShouldHas.customerList(), containsString("Customers")),
                seeThat(TheScreenShouldHas.driverList(), containsString("Drivers")),
                seeThat(TheScreenShouldHas.productsList(), containsString("Products")),
                seeThat(TheScreenShouldHas.trucksList(), containsString("Trucks"))
        );

    }

    @When("the user selects some options")
    public void theUserSelectsSomeOptions() {

        actor.attemptsTo(
                MaintenanceSomeOptions.maintenanceSelection()
        );

    }

    @Then("he can sees the selected results.")
    public void heCanSeesTheSelectedResults() {

        actor.should(
                seeThat(TheScreenShouldHas.driverList(), containsString("Drivers")),
                seeThat(TheScreenShouldHas.productsList(), containsString("Products")),
                seeThat(TheScreenShouldHas.trucksList(), containsString("Trucks"))
        );

    }

}
