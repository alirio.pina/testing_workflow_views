package com.cat2.sd;

import com.cat2.questions.TheScreenShouldHas;
import com.cat2.setup.SetUp;
import com.cat2.tasks.login.LogOut;
import com.cat2.tasks.login.SuccessfullyLogIn;
import com.cat2.tasks.login.UnsuccessfullyLogIn;
import com.cat2.tasks.logout.ExitLogIn;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

public class LogInSD extends SetUp {

    @Before
    public void before(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("the user is in the Log In screen.")
    public void theUserIsInTheLogInScreen() {

        actor.can(BrowseTheWeb.with(hisMobileDevice));

    }

    @When("inserts the correct credentials.")
    public void insertsTheCorrectCredentials() {

        actor.attemptsTo(
                SuccessfullyLogIn.successfullyLogIn()
        );

    }

    @Then("he can access in the menu.")
    public void heCanAccessInTheMenu() {

        actor.should(
                seeThat(TheScreenShouldHas.menuTitle(), containsString("DSD"))
        );

        actor.attemptsTo(
                LogOut.logOut()
        );

    }

    @When("inserts the incorrect credentials.")
    public void insertsTheIncorrectCredentials() {

        actor.attemptsTo(
                UnsuccessfullyLogIn.unsuccessfullyLogIn()
        );

    }

    @Then("he can not access in the menu.")
    public void heCanNotAccessInTheMenu() {

        actor.should(
                seeThat(TheScreenShouldHas.loginError(), containsString("Invalid PIN or User ID"))
        );

        actor.attemptsTo(
                ExitLogIn.exitLogIn()
        );

    }

}
