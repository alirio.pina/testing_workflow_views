package com.cat2.sd;

import com.cat2.questions.TheScreenShouldHas;
import com.cat2.setup.SetUp;
import com.cat2.tasks.customeroptions.NavigateToCustomerOptions;
import com.cat2.tasks.customerselect.NavigateToCustomerSelect;
import com.cat2.tasks.login.SuccessfullyLogIn;
import com.cat2.tasks.maintenance.MaintenanceAllOptions;
import com.cat2.tasks.startday.RegisterDaySuccesfully;
import com.cat2.tasks.startday.RegisterDayWithoutFields;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

public class StartDaySD extends SetUp {

    private static final String START_DAY_ERROR = "All required fields must be completed before the transaction can be successfully submitted";

    @Before
    public void before(){
        OnStage.setTheStage(new OnlineCast());
        actor.can(BrowseTheWeb.with(hisMobileDevice));
    }

    @Given("sync the maintenance")
    public void syncTheMaintenance() {

        actor.attemptsTo(
                SuccessfullyLogIn.successfullyLogIn(),
                MaintenanceAllOptions.maintenanceSelection()
        );

    }

    @When("the user enters in the start day without complete fields")
    public void theUserEntersInTheStartDayWithoutCompleteFields() {

        actor.attemptsTo(
                RegisterDayWithoutFields.registerDayWithoutFields()
        );
    }

    @Then("he cannot continue")
    public void heCannotContinue() {

        actor.should(
                seeThat(TheScreenShouldHas.startDayError(), containsString(START_DAY_ERROR))
        );

    }

    @When("the user enters in the start day screen")
    public void theUserEntersInTheStartDayScreen() {

        actor.attemptsTo(
                RegisterDaySuccesfully.registerDaySuccesfully()
        );

    }
    @Then("he can access at the customer select screen")
    public void heCanAccessAtTheCustomerSelectScreen() {

        actor.attemptsTo(

                NavigateToCustomerSelect.navigateToCustomerSelect()

        );

    }

    @When("the user enters at the start day")
    public void theUserEntersAtTheStartDay() {

        actor.attemptsTo(

                RegisterDaySuccesfully.registerDaySuccesfully()

        );

    }
    @When("selects an order")
    public void selectsAnOrder() {

        actor.attemptsTo(

                NavigateToCustomerSelect.navigateToCustomerSelect()

        );

    }
    @Then("he can access at the customer options screen")
    public void heCanAccessAtTheCustomerOptionsScreen() {

        actor.attemptsTo(

                NavigateToCustomerOptions.navigateToCustomerOptions()

        );

        actor.should(
                seeThat(TheScreenShouldHas.startDayError())
        );

    }

}
