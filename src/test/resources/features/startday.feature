Feature: Start Day

  Background:
    Given sync the maintenance

  Scenario: Start a day without fields completed
    When the user enters in the start day without complete fields
    Then he cannot continue

  Scenario: Navigate to customer select
    When the user enters in the start day screen
    Then he can access at the customer select screen

  Scenario: Navigate to customer options
    When the user enters at the start day
    And selects an order
    Then he can access at the customer options screen

