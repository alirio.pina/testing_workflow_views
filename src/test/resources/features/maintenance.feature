Feature: Maintenance

  Background:
    Given the user is in the maintenance screen.

  Scenario: All options selected.
    When the user selects all options.
    Then he can sees all the results.

  Scenario: Some options selected
    When the user selects some options
    Then he can sees the selected results.