package com.cat2.tasks.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.cat2.interaction.Wait.waitInSeconds;
import static com.cat2.ui.LogInOverview.*;
import static com.cat2.util.Constants.PASSWORD_CAT2;
import static com.cat2.util.Constants.USER_CAT2;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class SuccessfullyLogIn implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                WaitUntil.the(LOGO, isCurrentlyEnabled()).forNoMoreThan(8).seconds(),
                Enter.theValue(USER_CAT2).into(LOG_IN_FIELD),
                Enter.theValue(PASSWORD_CAT2).into(PASSWORD_FIELD),
                Click.on(LOG_IN_BTN),
                waitInSeconds(3000)

        );
    }

    public static SuccessfullyLogIn successfullyLogIn() {return new SuccessfullyLogIn(); }

}
