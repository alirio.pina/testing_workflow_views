package com.cat2.tasks.logout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static com.cat2.ui.LogInOverview.*;

public class ExitLogIn implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                Click.on(OK_INVALID_LOG_IN),
                Click.on(EXIT_LOG_IN),
                Click.on(OK_EXIT_LOGIN_IN_BTN)

        );
    }

    public static ExitLogIn exitLogIn() {return new ExitLogIn(); }

}
