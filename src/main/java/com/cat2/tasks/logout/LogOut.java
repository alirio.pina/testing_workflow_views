package com.cat2.tasks.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static com.cat2.ui.LogInOverview.LOGOUT_BTN;
import static com.cat2.ui.LogInOverview.LOGOUT_CONFIRM;

public class LogOut implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                Click.on(LOGOUT_BTN),
                Click.on(LOGOUT_CONFIRM)

        );
    }

    public static LogOut logOut() {return new LogOut(); }

}
