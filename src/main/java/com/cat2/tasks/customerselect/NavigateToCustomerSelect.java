package com.cat2.tasks.customerselect;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static com.cat2.ui.CustomerSelectOverview.*;

public class NavigateToCustomerSelect implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                Click.on(SEARCH_CAT),
                Click.on(SERVICED_CAT),
                Click.on(WITH_ORDER_CAT),
                Click.on(SEARCH_CAT),
                Enter.theValue("2").into(SEARCH_CAT_FIELD),
                Click.on(SEARCH_CAT_BTN),
                Click.on(SELECT_CUSTOMER),
                Click.on(CONTINUE_CUSTOMER_SELECT_BTN)

        );
    }

    public static NavigateToCustomerSelect navigateToCustomerSelect() {return new NavigateToCustomerSelect(); }
}
