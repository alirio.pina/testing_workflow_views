package com.cat2.tasks.customeroptions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.cat2.ui.CustomerOptionsOverview.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class NavigateToCustomerOptions implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                WaitUntil.the(DELIVER_BTN, isCurrentlyEnabled()).forNoMoreThan(8).seconds(),
                WaitUntil.the(CLOSE_BTN, isCurrentlyEnabled()).forNoMoreThan(8).seconds(),
                WaitUntil.the(CANCEL_BTN, isCurrentlyEnabled()).forNoMoreThan(8).seconds()

        );
    }

    public static NavigateToCustomerOptions navigateToCustomerOptions() {return new NavigateToCustomerOptions(); }

}
