package com.cat2.tasks.maintenance;

import com.cat2.interaction.Wait;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.cat2.ui.MaintenanceOverview.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class MaintenanceAllOptions implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                Click.on(MAINTENANCE_BTN),

                WaitUntil.the(LOGO_MAINTENANCE, isCurrentlyEnabled()).forNoMoreThan(8).seconds(),
                WaitUntil.the(TITLE_MAINTENANCE, isCurrentlyEnabled()).forNoMoreThan(8).seconds(),

                Click.on(SYNC_BTN),

                Wait.waitInSeconds(3000)

        );
    }

    public static MaintenanceAllOptions maintenanceSelection() {return new MaintenanceAllOptions(); }

}
