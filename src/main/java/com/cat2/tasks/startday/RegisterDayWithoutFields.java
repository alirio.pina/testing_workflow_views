package com.cat2.tasks.startday;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.cat2.ui.MaintenanceOverview.BACK_MAINTENANCE;
import static com.cat2.ui.StartDayOverview.*;
import static com.cat2.ui.StartUpViewOverview.START_DAY_BTN;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

public class RegisterDayWithoutFields implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                Click.on(BACK_MAINTENANCE),

                Click.on(START_DAY_BTN),

                Click.on(OPENED_DAY),

                WaitUntil.the(START_DAY_TITLE, isCurrentlyEnabled()).forNoMoreThan(8).seconds(),

                Click.on(CONTINUE_START_DAY_BTN)

        );
    }

    public static RegisterDayWithoutFields registerDayWithoutFields() {return new RegisterDayWithoutFields(); }

}
