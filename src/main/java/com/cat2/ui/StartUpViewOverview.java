package com.cat2.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class StartUpViewOverview {

    public static final Target START_DAY_BTN = Target
            .the("Start day btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeStartDay"));

    public static final Target RESUME_DAY_BTN = Target
            .the("Resume day btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeResumeDay"));

    public static final Target DELETE_DAY_BTN = Target
            .the("Delete day btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeDeleteDay"));

    public static final Target REPORTS_BTN = Target
            .the("Reports btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeReports"));

    public static final Target CLOSE_DAY_BTN = Target
            .the("Close day btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeCloseDay"));

    public static final Target UPLOAD_DAY_BTN = Target
            .the("Upload day btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeUploadDay"));

    public static final Target PRINTER_BTN = Target
            .the("Printer btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomePrinter"));

    public static final Target MAINTENANCE_BTN = Target
            .the("Maintenance btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeMaintenance"));

    public static final Target START_DAY_ERROR = Target
            .the("Start day error")
            .located(By.id("android:id/message"));

}
