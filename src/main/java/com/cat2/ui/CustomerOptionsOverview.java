package com.cat2.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class CustomerOptionsOverview {

    public static final Target DELIVER_BTN = Target
            .the("Deliver btn")
            .located(By.xpath("//android.widget.LinearLayout[@content-desc=\"Deliver\"]"));


    public static final Target CLOSE_BTN = Target
            .the("Close btn")
            .located(By.xpath("//android.widget.LinearLayout[@content-desc=\"Close\"]"));

    public static final Target CANCEL_BTN = Target
            .the("Cancel btn")
            .located(By.xpath("(//android.widget.LinearLayout[@content-desc=\"Cancel\"])[1]"));

    public static final Target ORDER_NUMBER = Target
            .the("Order number")
            .located(By.id("CAT2.Mobile.DSD:id/orderNumber"));

    public static final Target ORDER_ADDRESS = Target
            .the("Order address")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TableLayout/android.widget.TableRow[2]/android.widget.TextView[1]"));

}
