package com.cat2.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class StartDayOverview {

    public static final Target BACK_MAINT_BTN = Target
            .the("Back btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnSyncSummaryBack"));

    public static final Target TRUCK_ID_DD = Target
            .the("Truck id dropdown")
            .located(By.id("CAT2.Mobile.DSD:id/trunkId"));

    public static final Target ROUTE_ID_DD = Target
            .the("Route ID dropdown")
            .located(By.id("CAT2.Mobile.DSD:id/routeId"));

    public static final Target LOCATION_DD = Target
            .the("Location dropdown")
            .located(By.id("CAT2.Mobile.DSD:id/location"));

    public static final Target CONTINUE_START_DAY_BTN = Target
            .the("Continue startday btn")
            .located(By.id("CAT2.Mobile.DSD:id/continueButton"));

    public static final Target START_DAY_TITLE = Target
            .the("Continue startday btn")
            .located(By.id("CAT2.Mobile.DSD:id/start_day"));

    public static final Target ODOMETER = Target
            .the("Continue startday btn")
            .located(By.id("CAT2.Mobile.DSD:id/odometer"));

    public static final Target OPENED_DAY = Target
            .the("Opened day")
            .located(By.id("CAT2.Mobile.DSD:id/btnAlertDialogNegative"));

}
