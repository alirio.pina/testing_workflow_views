package com.cat2.ui;

import io.appium.java_client.AppiumBy;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class LogInOverview {

    public static final Target LOGO = Target
            .the("Logo")
            .located(By.id("CAT2.Mobile.DSD:id/ivLoginCat2Logo"));

    public static final Target LOG_IN_FIELD = Target
            .the("Login field")
            .located(AppiumBy.id("CAT2.Mobile.DSD:id/tietLoginUserId"));

    public static final Target PASSWORD_FIELD = Target
            .the("Password field")
            .located(By.id("CAT2.Mobile.DSD:id/tietLoginPin"));

    public static final Target LOG_IN_BTN = Target
            .the("Log in btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnLoginLogin"));

    public static final Target MENU_TITLE = Target
            .the("Menu title")
            .located(By.id("CAT2.Mobile.DSD:id/labelHomeDSD"));

    public static final Target LOGIN_ERROR = Target
            .the("Login error")
            .located(By.id("CAT2.Mobile.DSD:id/tvAlertDialogMessage"));

    public static final Target LOGOUT_BTN = Target
            .the("LogOut")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeLogout"));


    public static final Target LOGOUT_CONFIRM = Target
            .the("LogOut confirm")
            .located(By.id("CAT2.Mobile.DSD:id/btnAlertDialogPositive"));

    public static final Target OK_EXIT_LOGIN_IN_BTN = Target
            .the("Ok exit login btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnAlertDialogPositive"));

    public static final Target EXIT_LOG_IN = Target
            .the("Exit Log in")
            .located(By.id("CAT2.Mobile.DSD:id/btnLoginExit"));

    public static final Target OK_INVALID_LOG_IN = Target
            .the("Ok invalid log in")
            .located(By.id("CAT2.Mobile.DSD:id/btnAlertDialogPositive"));



}
