package com.cat2.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class MaintenanceOverview {

    public static final Target MAINTENANCE_BTN = Target
            .the("Maintenance btn")
            .located(By.id("CAT2.Mobile.DSD:id/btnHomeMaintenance"));

    public static final Target CUSTOMER_OPTION = Target
            .the("Customer option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceCustomer"));

    public static final Target ROUTES_OPTION = Target
            .the("Routes option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceRoutes"));

    public static final Target LOCATIONS_OPTION = Target
            .the("Locations option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceLocations"));

    public static final Target DRIVERS_OPTION = Target
            .the("Drivers option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceDrivers2"));

    public static final Target SHIP_RULES_OPTION = Target
            .the("Ship Rules option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceShipRules"));

    public static final Target VARIANCE_REASON_OPTION = Target
            .the("Variance reason option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceVarianceReasons"));

    public static final Target TRUCKS_OPTION = Target
            .the("Trucks option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceTrucks"));

    public static final Target PRODUCTS_OPTION = Target
            .the("Products option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceProducts"));

    public static final Target SYNC_BTN = Target
            .the("Sync btn")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/android.widget.Button"));

    public static final Target FORCE_ENTIRE_SYNC_OPTIONS = Target
            .the("Force Entire sync option")
            .located(By.id("CAT2.Mobile.DSD:id/cbMaintenanceForceEntireSync"));

    public static final Target LOGO_MAINTENANCE = Target
            .the("Logo Maintenance")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView"));

    public static final Target TITLE_MAINTENANCE = Target
            .the("Title maintenance")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView"));

    public static final Target TITLE_SYNC_SUMMARY = Target
            .the("title sync summary")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView"));

    public static final Target CUSTOMER_MAINT_LIST = Target
            .the("Customer list")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TableLayout/android.widget.TableRow[2]/android.widget.TextView[1]"));

    public static final Target DRIVER_MAINT_LIST = Target
            .the("Driver list")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TableLayout/android.widget.TableRow[4]/android.widget.TextView[1]"));

    public static final Target PRODUCTS_MAINT_LIST = Target
            .the("Products list")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TableLayout/android.widget.TableRow[6]/android.widget.TextView[1]"));

    public static final Target TRUCKS_MAINT_LIST = Target
            .the("Trucks list")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TableLayout/android.widget.TableRow[9]/android.widget.TextView[1]"));

    public static final Target BACK_MAINTENANCE = Target
            .the("Trucks list")
            .located(By.id("CAT2.Mobile.DSD:id/btnSyncSummaryBack"));

}
