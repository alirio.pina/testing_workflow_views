package com.cat2.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class CustomerSelectOverview {

    public static final Target WITH_ORDER_CAT = Target
            .the("With order category")
            .located(By.xpath("//android.widget.LinearLayout[@content-desc=\"With Orders\"]"));

    public static final Target SEARCH_CAT = Target
            .the("Search category")
            .located(By.xpath("//android.widget.LinearLayout[@content-desc=\"Search\"]"));

    public static final Target SERVICED_CAT = Target
            .the("Serviced category")
            .located(By.xpath("//android.widget.LinearLayout[@content-desc=\"Serviced\"]"));

    public static final Target SELECT_CUSTOMER = Target
            .the("Select customer")
            .located(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.CheckBox"));

    public static final Target CONTINUE_CUSTOMER_SELECT_BTN = Target
            .the("Select customer BTN")
            .located(By.id("CAT2.Mobile.DSD:id/continueButton"));

    public static final Target SEARCH_CAT_FIELD = Target
            .the("Search field")
            .located(By.id("CAT2.Mobile.DSD:id/textSearchField"));

    public static final Target SEARCH_CAT_BTN = Target
            .the("Search field BTN")
            .located(By.id("CAT2.Mobile.DSD:id/searchButton"));

}
