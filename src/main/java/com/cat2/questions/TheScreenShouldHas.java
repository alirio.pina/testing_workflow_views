package com.cat2.questions;

import com.cat2.ui.LogInOverview;
import com.cat2.ui.MaintenanceOverview;
import com.cat2.ui.StartUpViewOverview;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.targets.TheTarget;

public class TheScreenShouldHas {

   public static Question<String> menuTitle(){
        return TheTarget.textOf(LogInOverview.MENU_TITLE);
   }

   public static Question<String> loginError(){
        return TheTarget.textOf(LogInOverview.LOGIN_ERROR);
   }

   public static Question<String> customerList(){
        return TheTarget.textOf(MaintenanceOverview.CUSTOMER_MAINT_LIST);
   }

   public static Question<String> driverList(){
        return TheTarget.textOf(MaintenanceOverview.DRIVER_MAINT_LIST);
   }

   public static Question<String> productsList(){
        return TheTarget.textOf(MaintenanceOverview.PRODUCTS_MAINT_LIST);
   }

   public static Question<String> trucksList(){
        return TheTarget.textOf(MaintenanceOverview.TRUCKS_MAINT_LIST);
   }

   public static Question<String> startDayOption(){
        return TheTarget.textOf(StartUpViewOverview.START_DAY_BTN);
   }

   public static Question<String> resumeDayOption(){
        return TheTarget.textOf(StartUpViewOverview.RESUME_DAY_BTN);
   }

   public static Question<String> deleteDayOption(){
        return TheTarget.textOf(StartUpViewOverview.DELETE_DAY_BTN);
   }

   public static Question<String> reportsOption(){
        return TheTarget.textOf(StartUpViewOverview.REPORTS_BTN);
   }

   public static Question<String> closeDayOption(){
        return TheTarget.textOf(StartUpViewOverview.CLOSE_DAY_BTN);
   }

   public static Question<String> uploadDayOption(){
        return TheTarget.textOf(StartUpViewOverview.UPLOAD_DAY_BTN);
   }

   public static Question<String> printerOption(){
        return TheTarget.textOf(StartUpViewOverview.PRINTER_BTN);
   }

   public static Question<String> maintenanceOption(){
        return TheTarget.textOf(StartUpViewOverview.MAINTENANCE_BTN);
   }

   public static Question<String> startDayError(){
        return TheTarget.textOf(StartUpViewOverview.START_DAY_ERROR);
   }



    private TheScreenShouldHas() {
    }

}
