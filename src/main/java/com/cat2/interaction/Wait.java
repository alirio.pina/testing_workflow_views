package com.cat2.interaction;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Wait implements Interaction {

        private static int waitTime;

        @Override
        public <T extends Actor> void performAs(T actor) {
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

        }
        public static Wait waitInSeconds (int time) {
            waitTime = time;
            return instrumented(Wait.class);
        }

}
