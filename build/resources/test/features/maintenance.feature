Feature: Maintenance

  Background:
    Given the user is in the maintenance screen.

  Scenario: All options selected.
    When the user selects all options.
    And selects the sync button.
    Then he can sees all the results.

  Scenario: Some options selected
    Given the user selects some options
    When he selects the sync buttom.
    Then he can sees the selected results.