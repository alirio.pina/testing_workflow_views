Feature: Log In

  Background:
    Given the user is in the Log In screen.

  Scenario: Successful Log In.
    When inserts the correct credentials.
    Then he can access in the menu.

  Scenario: Unsuccessful Log In.
    When inserts the incorrect credentials.
    Then he can not access in the menu.